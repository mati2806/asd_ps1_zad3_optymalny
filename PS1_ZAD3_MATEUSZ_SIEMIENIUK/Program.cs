﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PS1_ZAD3_MATEUSZ_SIEMIENIUK
{
    class Program
    {
        static int iterations = 0;
        static void Main(string[] args)
        {
            string fileName="in4.txt";
            string[] words= new string[] { };
            var watch = System.Diagnostics.Stopwatch.StartNew();

            int[] bases = new int[] { };
            int size = 0;
            int max;
            int poprawna=0;

            if (fileName == "in1.txt") poprawna = 9;
            else if (fileName == "in2.txt") poprawna = 14;
            else if(fileName == "in3.txt") poprawna = 288;

            try { LoadFile(fileName, ref size, ref words, ref bases); }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Nie znaleziono pliku!");
                return;
            }

            MergeSort(ref bases, 0, size - 1);

            max = GetMax(ref size, ref bases);
            SaveFile(max);
            watch.Stop();


            Console.WriteLine("W czasie " + watch.ElapsedMilliseconds / 1000 + "s " + watch.ElapsedMilliseconds % 1000 + "ms.");
            Console.WriteLine("Ilosc baz: " + size);
            Console.WriteLine();
            Console.WriteLine("Otrzymana odpowiedz: " + max );
            if(poprawna > 0) Console.WriteLine("Poprawna odpowiedz: " + poprawna);
   
        }

        public static void LoadFile(string fileName, ref int size, ref string []words, ref int []bases)
        {
            int i = 0;
            int j = 1;

            StreamReader reader = new StreamReader(fileName);

            words = reader.ReadToEnd().Split(new[] { '\n', ' ' });

            size = Convert.ToInt32(words[0]);
            bases = new int[size];
            

            for(i=0; i < size; i++)
            {
                bases[i] = Convert.ToInt32(words[j]) * 60 + Convert.ToInt32(words[j + 1]); ;
                j = j + 2;

            }

        }

        public static void SaveFile(int max)
        {
            StreamWriter writer = new StreamWriter("out.txt");
            writer.WriteLine(max);
            writer.Close();
        }

        public static int GetMax(ref int size, ref int[] bases)
        {
            int i = 0;
            int j = 0;

            int maxAngle=21600;
            int max=0;
           
            int actual = 0;
            int start = 0;
            int end = size;

            
            for (i=0; i < size; i++)
            {

                actual = 0;
                maxAngle = bases[i] + 5400;

                start = i;
                end = size-1;

                while (start < end) //przeszukanie binarne
                {
                    iterations++;
                    j = (start + end) / 2;
                   
                    if (bases[j] <= maxAngle)
                    {
                        start = j + 1;
                    }
                    else if (bases[j] > maxAngle)
                    {
                        end = j - 1;
                    }
                }

                    actual = j - i + 1;  //zliczenie baz


                if (maxAngle>21600)
                {
                    
                    j = 0;
                    start = 0;
                    end = size-1;
                    
                    while (start < end) //przeszukanie binarne
                    {
                        iterations++;
                        j = (start + end) / 2;


                        if (bases[j] <= maxAngle-21600)
                        {
                            start = j + 1;
                        }
                        else if (bases[j] > maxAngle-21600)
                        {
                            end = j - 1;
                        }

                    }
                    
                    actual += j;  //dodanie granicznych baz
                }

                if (actual > max) max = actual;

            }

            Console.WriteLine("Wykonano w " + iterations + " petli.");
            return max;
        }

 
        public static void Merge(ref int []bases, int left, int middle, int right)
        {
            iterations++;
            int[] leftArray = new int[middle - left + 1];
            int[] rightArray = new int[right - middle];
            Array.Copy(bases, left, leftArray, 0, middle - left + 1);
            Array.Copy(bases, middle + 1, rightArray, 0, right - middle);

            int i = 0;
            int j = 0;
            for (int k = left; k < right + 1; k++)
            {
                if (i == leftArray.Length)
                {
                    bases[k] = rightArray[j];
                    j++;
                }
                else if (j == rightArray.Length)
                {
                    bases[k] = leftArray[i];
                    i++;
                }
                else if (leftArray[i] <= rightArray[j])
                {
                    bases[k] = leftArray[i];
                    i++;
                }
                else
                {
                    bases[k] = rightArray[j];
                    j++;
                }
            }
         

        }

        public static void MergeSort(ref int[] bases, int left, int right)
        {
            iterations++;
            if (left < right)
            {
                int middle = (left + right) / 2;

                MergeSort(ref bases, left, middle);
                MergeSort(ref bases, middle + 1, right);
                Merge(ref bases, left, middle, right);
            }
        }    


    }
}
